from modules import convert
from sodapy import Socrata
import os
import time
from datetime import datetime
from pathlib import Path
import json
from constants import DATASETS_PATH

def get_metatata(options):
    socrata_domain = options['socrata-domain']
    socrata_dataset_identifier = options['id']
    socrata_token = os.environ.get("SOCRATA_APPTOKEN")

    client = Socrata(socrata_domain, socrata_token)
    metadataResults = client.get_metadata(socrata_dataset_identifier)

    timestamp = metadataResults['rowsUpdatedAt']
    lastUpdated = datetime.fromtimestamp(timestamp)

    print('dataset last Updated:')
    print(lastUpdated)
    columns = []
    for item in  metadataResults['columns']:
        columns.append({
            'name': item['name'],
            'dataTypeName': item['dataTypeName'],
            'fieldName': item['fieldName'],
            'position': item['position']
        })

    metadata = {
        'id': metadataResults['id'],
        'name': metadataResults['name'],
        'description': metadataResults['description'],
        'sourceDomain':socrata_domain,
        'category': metadataResults['category'],
        'licenseId': metadataResults['licenseId'],
        'licenseName': metadataResults['license']['name'],
        'owner': metadataResults['owner']['displayName'],
        'attribution': metadataResults['attribution'],
        'lastUpdated': metadataResults['rowsUpdatedAt'],
        'columns': columns,
        'ownerImage':metadataResults['owner']['profileImageUrlLarge']
    }
    # check if the dataset has its own folder
    metadata_folder = '../' + DATASETS_PATH + options['folder']
    if not os.path.exists(metadata_folder):
        os.makedirs(metadata_folder)

    # check if the dataset was updated
    metadata_file = metadata_folder + '/'+ socrata_dataset_identifier
    metadata_file += '.metadata.json'

    metadata_file_old = Path(metadata_file)
    if metadata_file_old.is_file():# check if it exists before open it
        with open(metadata_file) as f:
            metadata_old = json.load(f)

            if (metadata['lastUpdated'] > metadata_old['lastUpdated']):
                # if it is updated, save the metadata
                write_metadata(metadata_file, metadata)
                # download the updated dataset
                get_query(options)
    else:
        # if there is not a metadata file create a new one
        write_metadata(metadata_file, metadata)
        get_query(options)

def write_metadata(metadata_file, metadata):
    print('... updating metadata')
    with open(metadata_file, 'w') as metafile:
        json.dump(metadata, metafile)

def get_query(options):
    q_options = options['query']

    if len(q_options) > 0:
        # get default query
        default_query = q_options[0]
        where_text = default_query['name']
        sub_folder = default_query['filters'][0]

        if (default_query['filter-type'] == 'string'):
            where_text += '="' + str(default_query['filters'][0]) +'"'
        else:
            where_text += '=' + str(default_query['filters'][0])
    # if there is and extra query
    if (len(q_options) == 2):
        extra_query = q_options[1]
        extra_query_filters = extra_query['filters']

        for filter in extra_query_filters:
            extra_query_text = ''
            extra_sub_folder = ''
            extra_sub_folder = '/' + filter

            if (extra_query['filter-type'] == 'string'):
                extra_query_text += ' and ' + extra_query['name'] +' = "' + filter + '"'
            else:
                extra_query_text += ' and ' + extra_query['name'] + ' = ' + filter

            options['where_text'] = where_text + extra_query_text
            options['sub_folder'] = sub_folder + extra_sub_folder
            get_dataset(options)
    # there is no query
    elif (len(options['query']) == 0):
        print('no query')
        options['where_text'] = ''
        get_dataset(options)
    # or just default query
    else:
        options['where_text'] = where_text
        options['sub_folder'] = sub_folder
        get_dataset(options)

def get_dataset(options):
    print('... updating dataset')
    socrata_domain = options['socrata-domain']
    socrata_dataset_identifier = options['id']
    socrata_token = os.environ.get("SOCRATA_APPTOKEN")
    client = Socrata(socrata_domain, socrata_token)

    print('query:')
    print(options['where_text'])
    results = client.get(socrata_dataset_identifier,
        where = options['where_text'],
        limit = 5000)

    print("Number of results downloaded: {}".format(len(results)))

    dest_folder = '../' + DATASETS_PATH + options['folder']
    if hasattr(options, 'sub_folder'):
        dest_folder += '/'+ str(options['sub_folder'])

    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    dest_file = dest_folder + '/' + socrata_dataset_identifier
    dest_file_json = dest_file +'.json'
    with open(dest_file_json, 'w') as outfile:
        json.dump(results, outfile)

    # convert dataset to geojson if it has a geolocation field
    if options['geolocation_field'] != '':
        convert.json2geojson(dest_file, options['geolocation_field'])

if __name__ == '__main__':
    get_metatata()
    get_dataset()
    write_metadata()
