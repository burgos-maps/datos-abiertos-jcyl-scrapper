import json
from geojson import Feature, FeatureCollection, Point, dump
import os

def json2geojson(file, geolocation_field):
    print('.. converting to geojson')
    features = []
    json_file = file + '.json'
    geojson_file = file + '.geojson'
    print(json_file)

    with open(json_file) as f:
        data = json.load(f)

        for evento in data:
            feature = {"type":"Feature"}
            feature["properties"] = evento
            feature["geometry"] = evento[geolocation_field]
            features.append(feature)
    feature_collection = FeatureCollection(features)

    with open(geojson_file, "w") as f:
       dump(feature_collection, f)

if __name__ == '__main__':
    json2geojson()
