from modules import socrata_importer, convert
import json
from sodapy import Socrata
import os
import time
from datetime import datetime

def openDatasetsList():
    with open('list.json') as f:
        data = json.load(f)
        for item in data['datasets']:
            print(item['nombre'] + ':')
            print('************************************************************')
            options = {
                'folder':item['folder'],
                'id': item['id'],
                'socrata-domain': item['socrata-domain'],
                'query': item['query'],
                'geolocation_field': item['geolocation_field']
            }
            socrata_importer.get_metatata(options)


if __name__ == '__main__':
    openDatasetsList()
